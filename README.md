# Control de Versiones con Git
En este curso se encuentran los recursos relacionados y tareas de la 
asignatura.
## Descripción
En este curso se abordan los conceptos básicos y las buenas prácticas 
para el control de versiones, así como el uso de la herramienta Git 
para facilitar la gestión de un repositorio de código fuente.
## Autor
* Ramírez Fuentes Edgar Alejandro
### Contacto
edgar.ramirez.fuentes.dev@gmail.com
## Instructor
* Suárez Espinoza Mario Alberto.
### Contacto
masues64@gmail.com